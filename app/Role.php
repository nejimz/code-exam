<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'roles';
    protected $guarded = [];

    public $timestamps = true;

    public function user()
    {
        return $this->belongsTo('App\User', 'id', 'role_id');
    }
}
