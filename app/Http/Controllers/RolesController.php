<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;

class RolesController extends Controller
{
    public function index(Request $request)
    {
        $page = 100;
        if ($request->has('page')) {
            $page = trim($request->page);
        }
        $rows = Role::paginate(100);
        return response()->json($rows);
    }
    
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:roles,name',
            'description' => 'required'
        ]);

        $row = Role::create($request->only('name', 'description'));

        return response()->json($row);
    }
    
    public function show($id)
    {
        $row = Role::whereId($id)->first();

        if (is_null($row)) {
            return response()->json(['message' => 'Role not found!']);
        }

        return response()->json($row);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|unique:roles,name,' . $request->id,
            'description' => 'required'
        ]);

        $row = Role::whereId($request->id)->update($request->only('name', 'description'));

        return response()->json($row);
    }
    
    public function destroy($id)
    {
        $row = Role::whereId($id)->delete();
        return response()->json(['message' => 'Role succesfully deleted!']);
    }
}
