<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Resources\UserResource;

class UsersController extends Controller
{
    public function index(Request $request)
    {
        $page = 100;
        if ($request->has('page')) {
            $page = trim($request->page);
        }
        $rows = User::with('role')->paginate(100);
        #return response()->json($rows);
        return UserResource::collection($rows)->response();
    }
    
    public function store(Request $request)
    {
        $request->validate([
            'full_name' => 'required',
            'email' => 'required|email|unique:users,email',
            'role_id' => 'required|exists:roles,id',
            'password' => 'required|min:8|same:password2'
        ]);
        $data = $request->only('full_name', 'email', 'role_id', 'password');
        $data['password'] = bcrypt($data['password']);
        #dd($data);
        $row = User::create($data);

        return response()->json($row);
    }
    
    public function show($id)
    {
        $row = User::whereId($id)->first();

        if (is_null($row)) {
            return response()->json(['message' => 'User not found!']);
        }

        return response()->json($row);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'full_name' => 'required',
            'email' => 'required|email|unique:users,email,' . $id,
            'role_id' => 'required|exists:roles,id',
            'password' => 'required|min:8|same:password2'
        ]);

        $data = $request->only('full_name', 'email', 'role_id', 'password');
        $data['password'] = bcrypt($data['password']);

        $row = User::whereId($id)->update($data);

        return response()->json($row);
    }
    
    public function destroy($id)
    {
        $row = User::whereId($id)->delete();
        return response()->json(['message' => 'User succesfully deleted!']);
    }
}
