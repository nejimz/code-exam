<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \GuzzleHttp\Client;
use Auth;

class LoginController extends Controller
{
    public function index()
    {
        return view('app');
    }

    public function authentication(Request $request)
    {
        #$credentials = $request->only('email', 'password');

        if (Auth::attempt($request->only('email', 'password'))) {
            return Auth::user();
        }

        return response()->json((object)['errors'=>['message'=>[0=>'Invalid Credentials!']]], 422);
    }
}
