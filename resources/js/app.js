
import Vue from 'vue' //Importing  
import BootstrapVue from 'bootstrap-vue' //Importing  
import router from './router' //Importing  
import App from './components/App' //Importing  

require('./bootstrap');

const app = new Vue({
    el: '#app',
    components: {
        App
    },
    router
});
