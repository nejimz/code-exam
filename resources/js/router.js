import Vue from 'vue';
import VueRouter from 'vue-router';
// components
import App from './components/App'  
import Login from './components/Login';
import Home from './components/Home';

import Users from './components/Users';
import UserCreate from './components/UserCreate';
import UserEdit from './components/UserEdit';

import Roles from './components/Roles';
import RoleCreate from './components/RoleCreate';
import RoleEdit from './components/RoleEdit';

import Logout from './components/Logout';

Vue.use(VueRouter);

export default new VueRouter({
	routes: [
		{
			path: '/', 
			name: 'app', 
			component: App,
			redirect: '/login'
		},
		{
			path: '/login', 
			name: 'login', 
			component: Login 
		},
		{
			path: '/home', 
			name: 'home', 
			component: Home
		},
		{
			path: '/users', 
			name: 'users', 
			component: Users
		},
		{
			path: '/users/create', 
			name: 'user-create', 
			component: UserCreate
		},
		{
			path: '/users/edit/:id', 
			name: 'user-edit', 
			component: UserEdit,
			params: true
		},

		{
			path: '/roles/', 
			name: 'roles', 
			component: Roles
			/*children: [
				{
					path: 'create', 
					name: 'roles-create', 
					component: RoleCreate 
				},
				{
					path: 'edit/:id', 
					name: 'roles-edit', 
					component: RoleEdit,
					params: true
				}
			]*/
		},
		{
			path: '/roles/create', 
			name: 'roles-create', 
			component: RoleCreate
		},
		{
			path: '/roles/edit/:id', 
			name: 'roles-edit', 
			component: RoleEdit,
			params: true
		},

		{
			path: '/logout', 
			name: 'logout', 
			component: Logout
		}
		/*,
		{
			path: '*', 
			name: 'notFound', 
			component: notFound 
		}*/
	],
	mode: 'history'
});