<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::resource('users', 'UsersController')->except(['create']);
Route::resource('roles', 'RolesController')->except(['create']);

Route::post('authentication', 'LoginController@authentication')->name('login.authentication');

#Route::post('users/store', 'UsersController@store')->name('users.store');
#Route::post('users/update/{id}', 'UsersController@update')->name('users.update');

